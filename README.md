# Histogram Clustering & Unsupervised Segmentation

This repo contains a partial implementation of **Histogram Clustering** used to
perform unsupervised image segmentation of textures. The algorithm is
implemented following the **Sklearn** API.

NOTE: This algorithm is partial work, further revisions are needed.

### References

* Histogram clustering for unsupervised image segmentation:
  http://ieeexplore.ieee.org/document/784981/
