import sklearn as skl
import matplotlib.pyplot as plt
import numpy as np

from sklearn.utils.validation import check_is_fitted
from sklearn.feature_extraction import image
from matplotlib.image import imread


def random_initialize_clusters_assignments(image, num_clusters):
    """
    Initialize the cluster cluster_assigments vector to random values
    """

    # For each pixel assign a random cluster
    n_samples = image.shape[0] * image.shape[1]
    
    categorical_values = list(range(1,num_clusters+1))

    random_assigments = np.random.choice(categorical_values, n_samples)

    return random_assigments


def return_padded_image(image, window_size):
    
    height_pad = window_size // 2
    width_pad = height_pad

    padded_image = np.pad(image, ((height_pad, height_pad), (width_pad, width_pad)), mode='constant')

    return padded_image


def compute_patch(padded_image, i, j, patch_size):
    """Return patch with hight as number of pixels surrounding the i,j location
    and width as the number of pixels surrounding i,j
    
    i: Height position, row in image matrix (y)
    j: Width position, column in image matrix (x)
    patch_size: tuple (height_patch, width_patch)
    """

    height_patch, width_patch = patch_size

    if height_patch % 2 == 0 or width_patch % 2 == 0:
        raise ValueError("Width and height should be odd numbers")

    
    height_pad = height_patch // 2
    width_pad  = width_patch // 2
    
    padded_array_index_i = i + height_pad
    padded_array_index_j = j + width_pad

    rows = list(range(padded_array_index_i - height_pad, (padded_array_index_i + height_pad) +1))
    columns = list(range(padded_array_index_j - width_pad, (padded_array_index_j + width_pad) + 1))

    patch = padded_image[rows][:, columns]


    if patch.shape != (height_patch, width_patch):
        raise ValueError("Incorrect patch computation")

    return patch


def compute_array_of_patches(image, window_size):
    """Matrix with the same of size as X"""

    n_rows, n_cols = image.shape
    n_pixels = n_rows*n_cols


    padded_image = return_padded_image(image, window_size)
    patch_size = (window_size, window_size)

    array_of_patchs = []

    for pixel_index in range(n_pixels):
        print("Computing patch for pixel {}".format(pixel_index), end='\r')
        i,j = np.unravel_index(pixel_index, (n_rows, n_cols))
        array_of_patchs.append(compute_patch(padded_image, i, j, patch_size))


    return array_of_patchs

def compute_histogram_for_patch(patch_array, n_bins):

    patch_histogram = np.histogram(patch_array.ravel(), bins=n_bins) # Gray Scale Histogram
    # I should check the len of the array of values

    if patch_histogram[0].shape[0] != n_bins:
        raise ValueError("Incorrect size for the histogram")

    return patch_histogram[0]


def compute_py_given_x(patch_x, n_bins):

    histogram = compute_histogram_for_patch(patch_x, n_bins)

    # Normalize histogram to sum 1
    py_given_x = histogram/sum(histogram)


    return py_given_x

def compute_array_of_py_given_x(X, n_bins, array_of_patches):

    array_of_py_given_x = []

    for x_index, x in enumerate(X):
        print("Computing histogram for pixel {}".format(x_index), end='\r')
        patch_x = array_of_patches[x_index]
        array_of_py_given_x.append(compute_py_given_x(patch_x, n_bins))

    return array_of_py_given_x

def compute_nx(window_size):
    hp = window_size
    wp = window_size

    return hp*wp

def compute_px(X):
    return (1/len(X))


def compute_py_given_c(X, c, cluster_assigments, array_of_patches, array_of_py_given_x, window_size, n_bins):
    """
    Returns a vector with py_c for each y
    """

    py_given_c = np.zeros(n_bins)

    observations_with_cluster_c_indexes = np.argwhere(cluster_assigments == c).reshape(-1)
    # observations_with_cluster_c = X[observations_with_cluster_c_indexes]

    for x_index in observations_with_cluster_c_indexes:
        py_given_x = array_of_py_given_x[x_index]
        py_given_c = py_given_c + py_given_x

    return py_given_c * (1/len(observations_with_cluster_c_indexes))

def compute_pc(num_clusters):

    # Assuming no cluster class imbalance
    pc = 1/num_clusters

    return pc


def compute_min_term_for_given_a(x_index, array_of_py_given_x, n_bins, py_c):

    py_given_x = array_of_py_given_x[x_index]

    first_term = 0

    for y_index in range(n_bins):
        term = py_given_x[y_index] * np.log(py_c[y_index])
        first_term = first_term + term
    
    result = -(first_term)

    return result


def compute_cx(x_index, array_of_patchs, array_of_py_given_x, window_size, n_bins, centroids):

    n_clusters = centroids.shape[0]
    y_features = centroids.shape[1]


    argmin = 1
    term_min = compute_min_term_for_given_a(x_index, array_of_py_given_x, n_bins, np.reshape(centroids[0], -1))

    for centroid_index in range(1, n_clusters):

        centroid = centroids[centroid_index]
        # Ensure that the centroid that we pass is a 1-d Array

        next_term = compute_min_term_for_given_a(x_index, array_of_py_given_x, n_bins, np.reshape(centroid, -1)) 

        if next_term <= term_min:
            argmin = centroid_index + 1
            term_min = next_term

    return argmin

def compute_centroids(X, cluster_assigments, array_of_patchs, array_of_py_given_x, window_size, n_clusters, n_bins):
    """
    Compute the initial centroids given the random cluster_assigments
    """

    centroids = []

    for cluster_index in range(n_clusters):
        print("Computing centroid {}".format(cluster_index))
        py_given_c = compute_py_given_c(X, cluster_index+1, cluster_assigments, array_of_patchs, array_of_py_given_x, window_size, n_bins)
        centroids.append(py_given_c)

    
    return np.stack(centroids)

def compute_likelihood(X, cluster_assigments, array_of_patches, array_of_py_given_x, centroids):

    nx = compute_nx(array_of_patches[0].shape[0])
    px = compute_px(X)
    n_bins = centroids.shape[1]

    first_sum = 0
    for x_index, x in enumerate(X):
        
        py_x = array_of_py_given_x[x_index]
        c_x = cluster_assigments[x_index]

        py_c = centroids[c_x - 1] # Centroids start at 1

        inside_sum = 0

        for y_index in range(n_bins):
            term = py_x[y_index] * np.log(py_c[y_index])
            inside_sum = inside_sum + term
       
        first_sum = first_sum  +  (inside_sum + np.log(px))
    
    return first_sum * nx


class HistogramClustering(skl.base.BaseEstimator, skl.base.TransformerMixin):
    """Template class for HistogramClustering (HC)
    
    Attributes:
        centroids (np.ndarray): Array of centroid distributions p(y|c) with shape (n_clusters, n_bins).
        
    Parameters:
        n_clusters (int): Number of clusters (textures).
        n_bins (int): Number of bins used to discretize the range of pixel values found in input image X.
        window_size (int): Size of the window used to compute the local histograms for each pixel.
                           Should be an odd number larger or equal to 3.
        random_state (int): Random seed.
        estimation (str): Whether to use Maximum a Posteriori ("MAP") or
                          Deterministic Annealing ("DA") estimation.
    """
    
    def __init__(self, n_clusters=10, n_bins=64, window_size=7, random_state=42, estimation="MAP"):
        self.n_clusters = n_clusters
        self.n_bins =n_bins
        self.window_size = window_size
        self.random_state = random_state
        self.estimation = estimation
        self.centroids = None
        # Add more parameters, if necessary.
    
    def fit(self, image):
        """Compute HC for input image X
        
        Compute centroids.        
        
        Args:
            X (np.ndarray): Input array with shape (height, width)
        
        Returns:
            self
        """

        
        cluster_assigments = random_initialize_clusters_assignments(image, self.n_clusters)
        array_of_patches = compute_array_of_patches(image, self.window_size)

        X = image.ravel()
        array_of_py_given_x = compute_array_of_py_given_x(X, self.n_bins, array_of_patches)

        # Compute initial centroid
        centroids = compute_centroids(X,
                                      cluster_assigments,
                                      array_of_patches,
                                      array_of_py_given_x,
                                      self.window_size,
                                      self.n_clusters,
                                      self.n_bins)



        for i in range(4):

            for x_index, x in enumerate(X):
                cluster_assigments[x_index] = compute_cx(x_index, array_of_patches, array_of_py_given_x, self.window_size, self.n_bins, centroids)
                print("Updating cluster assigment for pixel {}".format(x_index), end='\r')

            centroids = compute_centroids(X,
                                          cluster_assigments,
                                          array_of_patches,
                                          array_of_py_given_x,
                                          self.window_size,
                                          self.n_clusters,
                                          self.n_bins)

        self.centroids = centroids
    
    def predict(self, image):
        """Predict cluster assignments for each pixel in image X.
        
        Args:
            X (np.ndarray): Input array with shape (height, width)
            
        Returns:
            C (np.ndarray): Assignment map (height, width) 
        """
        check_is_fitted(self, ["centroids"])


        array_of_patches = compute_array_of_patches(image, self.window_size)

        X = image.ravel()

        array_of_py_given_x = compute_array_of_py_given_x(X, self.n_bins, array_of_patches)

        cluster_assigments = np.empty(X.shape[0])

        for x_index in range(X.shape[0]):
            cluster_assigments[x_index] = compute_cx(x_index, array_of_patches, array_of_py_given_x, self.window_size, self.n_bins, self.centroids)

        
        C = np.reshape(cluster_assigments, (image.shape[0], image.shape[1]))

        return C
    
    def generate(self, C):
        """Generate a sample image X from a texture label map C.
        
        The entries of C are integers from the set {1,...,n_clusters}. They represent the texture labels
        of each pixel. Given the texture labels, a sample image X is generated by sampling
        the value of each pixel from the fitted p(y|c).
        
        Args:
            C (np.ndarray): Input array with shape (height, width)
            
        Returns:
            X (np.ndarray): Sample image (height, width)
        """
        check_is_fitted(self, ["centroids"])
        
        # Your code goes here
        
        return X

def load_texture():
    texture = imread("texture.png")

    patch_grid_shape = (2,5)
    patch_shape = (128,128)
    label_map = np.ones(texture.shape)
    for patch in range(np.prod(patch_grid_shape)):
        i, j = np.unravel_index(patch, patch_grid_shape)
        label_map[patch_shape[0] * i : patch_shape[0] * (i + 1), \
                  patch_shape[1] * j : patch_shape[1] * (j + 1)] = np.ravel_multi_index((i,j), patch_grid_shape)

    plt.imshow(texture)
    plt.title("Textures")
    plt.figure()
    plt.imshow(label_map, cmap="tab20")
    plt.title("Labels")
    plt.show()

    return texture

def permuted_texture(texture):
    perm_patch_grid_shape = tuple(i * 2 for i in patch_grid_shape)
    perm_patch_shape = tuple(i//2 for i in patch_shape)
    n_perm_patches = np.prod(patch_grid_shape) * 4

    np.random.seed(5)
    perm = np.random.permutation(n_perm_patches)

    perm_texture = np.ones(texture.shape)
    perm_label_map = np.ones(texture.shape)

    for patch, perm_patch in enumerate(perm):
        i, j = np.unravel_index(patch, perm_patch_grid_shape)
        ip, jp = np.unravel_index(perm_patch, perm_patch_grid_shape)
        
        perm_texture[ip * perm_patch_shape[0] : (ip + 1) * perm_patch_shape[0], \
                     jp * perm_patch_shape[1] : (jp + 1) * perm_patch_shape[1]] = \
        texture[i * perm_patch_shape[0] : (i + 1) * perm_patch_shape[0], \
                j * perm_patch_shape[1] : (j + 1) * perm_patch_shape[1]]
        
        perm_label_map[ip * perm_patch_shape[0] : (ip + 1) * perm_patch_shape[0], \
                       jp * perm_patch_shape[1] : (jp + 1) * perm_patch_shape[1]] = \
        label_map[i * perm_patch_shape[0] : (i + 1) * perm_patch_shape[0], \
                  j * perm_patch_shape[1] : (j + 1) * perm_patch_shape[1]]
        
    plt.imshow(perm_texture)
    plt.title("Permuted Textures")
    plt.figure()
    plt.imshow(perm_label_map, cmap="tab20")
    plt.title("Permuted Labels")
    plt.show()
